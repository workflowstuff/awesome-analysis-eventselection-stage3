FROM rootproject/root-conda
ADD . /analysis/skim
WORKDIR /analysis/skim
RUN echo ">>> Compile skimming executable ..." &&  \
    COMPILER=$(root-config --cxx) &&  \
    FLAGS=$(root-config --cflags --libs) &&  \
    $COMPILER -g -std=c++11 -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS



